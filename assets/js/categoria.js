var categoria = (function() {
	var C = {};

	C.setDHTML = function() {
		
		$('#form_post_categoria').submit( function(e){
			e.preventDefault();
		});

		$('#btnSalvar').on('click', function() {
			C.salvar();
		});

		$("#listaCategoria").tablesorter({
			theme: 'blue',
			widgets: ["filter", "zebra"],
			widgetOptions: {
				filter_columnFilters: false,
				filter_saveFilters: true,
				filter_reset: '.reset'
			},
			headers: { 
				2: { 
					sorter: false 
				}, 
			}
		}); 	

	};

	C.salvar = function() {

		var nm_categoria = $('#nm_categoria').val();
		var id_categoria = $('#id_categoria').val();
		var error = false;

		if (nm_categoria == '') {
			msg = "Preencha o nome da categoria!";
			script.alertError(msg);
			error = error ? error : true;
		}

		if(!error){
			var data = {
				nm_categoria:nm_categoria,
				id_categoria: id_categoria
			};

			$.post('categoria_post.php',data, function(ret){
				
				if(ret.res == 'ok'){
					script.alertSucesso(ret.msg, ret.url); 
				}
				else{
					script.alertError(ret.msg);
				}

			}, 'json');
		}
	}

	$(function() {
		C.setDHTML();
	});

	return C;
})();