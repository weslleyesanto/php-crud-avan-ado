var perfil = (function() {
	var P = {};
	var files;
	var x1, y1, width, height;

	P.setDHTML = function() {

		P.getTags();

		$("#dt_nascimento").mask("99/99/9999");
		
		$('#form_post_perfil').submit( function(e){
			e.preventDefault();
		});

		$('input[type=file]').on('change', function(event){
			files = event.target.files;
		});

		$('#btnSalvar').on('click', function() {
			P.salvar();
		});

		/*INICIO AÇÃO DE BUSCAR */
		$("#pesquisar").on('keypress keyup', function(){
			var pesquisar = $(this).val();
			$('#exportar_excel').removeAttr('href').attr({'href':'exportar.php?tipo=xls&pesquisar='+pesquisar});
			$('#exportar_txt').removeAttr('href').attr({'href':'exportar.php?tipo=txt&pesquisar='+pesquisar});		
		});
		/*FINAL AÇÃO DE BUSCAR */

		$('#listaPerfil').on('click', '.notificar', function(){
			var id_perfil = $(this).parents('tr').attr('data-id');
			P.notificar(id_perfil);

		});

		/* INICIO LISTAGEM DINAMICA*/
		var $table = $('#listaPerfil').tablesorter({
			theme: 'blue',
			widgets: ["filter", "zebra"],
			widgetOptions: {
				filter_columnFilters: false,
				filter_saveFilters: false,
				filter_reset: '.reset'
			},
			headers: { 
				9: { 
					sorter: false 
				}, 
			}
		});

		$.tablesorter.filter.bindSearch($table, $('.search'));
		/* FINAL LISTAGEM DINAMICA */

		/*INICIO DATE PICKER*/
		$( "#dt_nascimento" ).datepicker({
			dateFormat: 'dd/mm/yy',
			dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			nextText: 'Próximo',
			prevText: 'Anterior'}
			);

		/*FINAL DATE PICKER*/

		//ATRIBUINDO CHOSEN EM ID CATEGORIA
		$('#id_categoria').chosen();

		$('#id_tipo').on('change', function(){
			P.buscarSubtipo();
		});


		/*INICIO CROP IMAGE*/
		$('#foto').on('change', function() {
			
			var jcrop_api;

	    	//DESTROI PARA CONSTRUIR DE NOVO
	    	if ($('#Image1').data('Jcrop')) {
	    		jcrop_api = $('#Image1').data('Jcrop');
	    		jcrop_api.destroy();
	    	};

	    	var imagem = $('#Image1').attr('src');

	    	$('.jcrop-holder').load();
	    	$('#canvas').attr({'height': '5', 'width':'5'});
	    	$('#btnCrop').removeAttr('style').css({'display':'none'});

	    	$('#Image1').hide();

	    	var reader = new FileReader();
	    	reader.onload = function (e) {
	    		$('#Image1').show();
	    		$('#Image1').attr("src", e.target.result);
	    		$('#Image1').Jcrop({
	    			onChange: SetCoordinates,
	    			onSelect: SetCoordinates
	    		});
	    	}
	    	reader.readAsDataURL($(this)[0].files[0]);
	    });

		$('#btnCrop').click(function () {
			x1 = $('#imgX1').val();
			y1 = $('#imgY1').val();
			width = $('#imgWidth').val();
			height = $('#imgHeight').val();
			
			var canvas = $("#canvas")[0];
			var context = canvas.getContext('2d');
			var img = new Image();

			img.onload = function () {
				canvas.height = height;
				canvas.width = width;
				context.drawImage(img, x1, y1, width, height, 0, 0, width, height);
				$('#imgCropped').val(canvas.toDataURL());
				$('[id*=btnUpload]').show();
			};
			img.src = $('#Image1').attr("src");
		});

		function SetCoordinates(c) {
			$('#imgX1').val(c.x);
			$('#imgY1').val(c.y);
			$('#imgWidth').val(c.w);
			$('#imgHeight').val(c.h);
			$('#btnCrop').show();
		};
		/*FINAL CROP IMAGE*/

		var id_tipo = $('#id_tipo').val();

		if(id_tipo != ''){
			P.buscarSubtipo();
		}

	};

	P.getTags = function(){
		$("#form_post_perfil").find('input.tags').tagedit({
			autocompleteURL: 'tags_get.php'
		});
	}

	P.buscarSubtipo = function(){
		
		var id_tipo = $('#id_tipo').val();
		var id_subtipo_perfil = $('#id_subtipo').attr('data-id');
		
		var data = {}
		data['id_tipo'] = id_tipo;

		if(id_subtipo_perfil != "0"){
			data['id_subtipo_perfil'] =  id_subtipo_perfil;
		}

		$.post('subtipo_buscar.php', data, function(ret){
			if(ret.res == "ok"){

				$('#id_subtipo').html("");
				$('#id_subtipo').html(ret.OPTION);

				return false;	
			}else if(ret.res == "error"){
				script.alertError(ret.msg);
			}
		}, 'json');

	}

	P.salvar = function(event) {

		var nm_perfil = $('#nm_perfil').val();
		var email = $('#email').val();
		var dt_nascimento = $('#dt_nascimento').val();
		var foto = $('#foto').attr('ds-foto');
		var value_foto = $('#foto').val();
		var caminho_foto = $('#caminho_foto').attr('src');
		var id_categoria = $('#id_categoria').val();
		var id_tipo = $('#id_tipo').val();
		var id_subtipo = $('#id_subtipo').val();
		var tags = $('input[type="hidden"][name^="tag"]').val();
		var id_perfil = $('#id_perfil').val();
		
		var v = grecaptcha.getResponse();

		var error = false;

		if (nm_perfil == '') {
			msg = "Preencha o nome!";
			script.alertError(msg);
			error = error ? error : true;
		}else if (email == '') {
			msg = "Preencha o E-mail!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(!P.validateEmail(email)){
			msg = "Preencha o E-mail corretamente!";
			script.alertError(msg);
			error = error ? error : true;
		}else if (dt_nascimento == '') {
			msg = "Preencha a data de nascimento!";
			script.alertError(msg);
			error = error ? error : true;
		}else if ((value_foto == '' || typeof value_foto == 'undefined') //ADD UM NOVO PERFIL
			&& (caminho_foto == '' || typeof caminho_foto == 'undefined')){
			msg = "Inclua uma imagem!"; 
			script.alertError(msg);
			error = error ? error : true;
		}else if ((value_foto != '' || typeof value_foto != 'undefined') //ADD UM NOVO PERFIL, CORTAR IMAGEM
			&& (caminho_foto == '' || typeof caminho_foto == 'undefined')
			&& (x1 == '' || typeof x1 === 'undefined')
			&& (y1 == '' || typeof y1 === 'undefined')
			&& (width == '' || typeof width === 'undefined')
			&& (height == '' || typeof height === 'undefined')){
			msg = "Corte a imagem!";
			script.alertError(msg);
			error = error ? error : true;
		}else if (value_foto != ''
			&& (caminho_foto != '' || typeof caminho_foto != 'undefined')
			&& (x1 == '' || typeof x1 === 'undefined')
			&& (y1 == '' || typeof y1 === 'undefined')
			&& (width == '' || typeof width === 'undefined')
				&& (height == '' || typeof height === 'undefined')){ //ALTERANDO UM PERFIL TROCANDO IMAGEM
			msg = "Corte a imagem!";
			script.alertError(msg);
			error = error ? error : true;
		}else if (id_categoria == '' || id_categoria == null) {
			msg = "Selecione uma categoria!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(id_tipo == ""){
			msg = "Selecione um tipo!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(id_subtipo == ""){
			msg = "Selecione um subtipo!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(typeof tags === 'undefined'){
			msg = "Insira uma tag!";
			script.alertError(msg);
			error = error ? error : true;
		}else if(v.length == 0){
			msg = "Campo captcha obrigatório!";
			script.alertError(msg);
			error = error ? error : true;
		}


		if(!error){

			var data = new FormData();

			if(typeof files != 'undefined'){
				$.each(files, function(key, value)
				{
					data.append('foto', value);
				});
			}

			data.append('nm_perfil', nm_perfil);

			data.append('ds_foto', foto);
			data.append('x1', x1); 
			data.append('y1', y1); 
			data.append('width', width); 
			data.append('height', height);

			data.append('email', email);
			data.append('dt_nascimento', dt_nascimento);
			data.append('id_categoria', id_categoria);
			data.append('id_tipo', id_tipo);
			data.append('id_subtipo', id_subtipo);

			var tags_name = $('input[type="hidden"][name^="tag"]').map(function(){
				return this.value;
			}).get();

			data.append('nm_tags', tags_name);
			
			data.append('id_perfil', id_perfil);

			$.ajax({
				url: "perfil_post.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				dataType: 'json',
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					if(data.res == 'error_email'){
						script.alertError(data.msg);
					}else if(data.res == 'ok'){
						script.alertSucesso(data.msg, data.url);
					}else if(data.res == 'error'){
						script.alertError(data.msg);
					}
				},
				error: function(data){
					script.alertError(data.msg);
					return false;
				}
			});
		}

	}

	P.notificar = function(id_perfil){

		var data = {
			id_perfil: id_perfil
		};

		$.post('notificar.php', data, function(ret){
			if(ret.res == "ok"){
				script.alertSucesso(ret.msg, 'default.asp'); 
			}else if(ret.res == "error"){
				script.alertError(ret.msg);
			}
		}, 'json');


		return false;
	}

	P.validateEmail = function (email)
	{
		//er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
		er = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
		if(er.exec(email)){
			return true;
		}else{
			return false;
		}
	};

	P.pesquisar = function(){
		var pesquisar = $('#pesquisar').val();
		var data ={
			pesquisar: pesquisar
		}

		$.post('pesquisar.php', data, function(ret){
			if(ret.res == "ok"){
				$('#listaPerfil tbody').html("");
				$('#listaPerfil tbody').html(ret.TBODY);

				return false;	
			}else if(ret.res == "error"){
				script.alertError(ret.msg);
				return false
			}
		}, 'json');
	};

	$(function() {
		P.setDHTML();
	});

	return P;
})();