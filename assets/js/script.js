var script = (function() {
	var S = {}, winH, winW;

	S.setDHTML = function() {

		$('#listaPerfil').on('click','.excluir',function(){

			var modulo = $(this).parents('tr').attr('modulo');
			var page = $(this).parents('tr').attr('page');
			var confirmar = confirm("Deseja realmente exluir esse(a) "+modulo+"?");

			if (confirmar) {
				var id = $(this).parents('tr').attr('data-id');
				S.excluir(modulo, id, page);
				return false;
			} 			
		});

		$('.excluir').on('click', function(){
			
			var modulo = $(this).parents('tr').attr('modulo');
			var page = $(this).parents('tr').attr('page');
			var confirmar = confirm("Deseja realmente exluir esse(a) "+modulo+"?");

			if (confirmar) {
				var id = $(this).parents('tr').attr('data-id');
				$(this).parents('tr').fadeOut(); //esconde a tr antes de atualizar a página
				S.excluir(modulo, id, page);
				return false;
			} 			
		});

		 $(".chosen-select").chosen({no_results_text: "Oops, nada encontrado!"}); 

	};


	S.excluir = function(modulo, id, page){

		var data = {
			modulo: modulo,
			page: page,
			id: id
		};


		$.post('excluir.php', data, function(ret){

			if(ret.res == "ok"){
				S.alertSucesso(ret.msg, ret.url);
			}else if(ret.res == "error"){
				S.alertSucesso(ret.msg, ret.url);
			}
		}, 'json');


		return false;
	}

	S.alertError = function(msg){
		$('#alert')
			.addClass('alert-danger')
			.css({'display':'block'})
			.html('<strong style="color:red">Error!</strong> '+msg);
	}

	S.alertSucesso = function(msg, url){
		$('#alert')
		.addClass('alert-success')
		.css({'display':'block'})
		.html('<strong style="color:green">OK!</strong> '+msg);

		if(url != false){
			setTimeout(function(){ window.location.href = url; }, 1000);
		}
		return false;
	}


	$(function() {
		S.setDHTML();
	});

	return S;
})();