var subcategoria = (function() {
	var SC = {};

	SC.setDHTML = function() {
		
		$('#form_post_subcategoria').submit( function(e){
			e.preventDefault();
		});

		$('#btnSalvar').on('click', function() {
			SC.salvar();
		});

		$("#listaSubcategoria").tablesorter({
			headers: { 
            	3: { 
                	sorter: false 
            		}, 
        	}
		}); 	

	};

	SC.salvar = function() {

		var nm_subcategoria = $('#nm_subcategoria').val();
		var id_subcategoria = $('#id_subcategoria').val();
		var id_categoria = $('#id_categoria').val();
		var error = false;

		if (id_categoria == '') {
			msg = "Selecione uma categoria!";
			SC.alertError(msg);
			error = error ? error : true;
		}else if (nm_subcategoria == '') {
			msg = "Preencha o nome da subcategoria!";
			SC.alertError(msg);
			error = error ? error : true;
		}

		if(!error){
			var data = {
				id_subcategoria: id_subcategoria,
				nm_subcategoria: nm_subcategoria,
				id_categoria: id_categoria
			};
			//console.log(data);

			$.post('subcategoria_post.php',data, function(ret){
				
				if(ret.res == 'ok'){
					alert(ret.msg);
					window.location.href = ret.url;
				}
				else if(ret.res == 'error'){
					SC.alertError(ret.msg);
					return false;
				}

			}, 'json');
		}
	}


	$(function() {
		SC.setDHTML();
	});

	return SC;
})();