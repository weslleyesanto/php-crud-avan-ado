<?php
include_once('include\config.php');

$CATEGORIA = "categoria_list.php";

if($_GET["q"] == "s"){ 

    if($_GET["acao"] == "add"){
        $title = "Adicionar Categoria";
        $nm_categoria = "";
        $id_categoria = "0";
    }elseif ($_GET["acao"] == "alterar" AND $_GET["id"] != ""){
        
        if(!is_numeric($_GET["id"])){
           header("location:{$CATEGORIA}");
       } else{
        $title = "Alterar Categoria";

        $id_categoria = limpar($_GET["id"]);
        //QUERY VERIFICAR SE CATEGORIA EXISTEM COM ESSE ID'
        $TABELA = "tb_categoria ";
        $WHERE = " WHERE id_categoria = ". $id_categoria;
                                //$conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
        $execute_select = select($conn, $TABELA, false, $WHERE, false, false, false, false, false);
        
        if($execute_select->rowCount() > 0){//VERIFICA SE TRUE

            foreach ($execute_select as $row) {
                $id_usuario = $row["id_categoria"];
                $nm_categoria = utf8_encode($row["nm_categoria"]);
            }
            //FINAL FOREACH

        }else{//SE NÃO ENCONTROU VOLTA PRA LISTAGEM'
        header("location:{$CATEGORIA}");
        //FINAL VERIFICA SE RETORNOU ALGO DO SELECT
    }
}
    //FINAL VERIFICA SE É UM NÚMERO O ID
}else{
    header("location:{$CATEGORIA}");
}
    //FINAL IF TIPO DE AÇÃO'
}else{
    header("location:{$CATEGORIA}");
}
//FINAL IF Q IGUAL A S' 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$title?></title>

</head>

<body>

    <div id="wrapper">

        <?php include_once('include\menu.php'); ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <h1><?=$title?></h1>

                <div id="alert" style="display:none;"> </div>

                <form role="form" name="form_post_categoria" method="post" id="form_post_categoria">

                    <label>Nome Categoria</label>
                    <input type="text" required="required" placeholder="Insira uma nova categoria" id="nm_categoria" name="nm_categoria" value="<?=$nm_categoria?>" />
                    <input type="hidden" id="id_categoria" name="id_categoria" value="<?=$id_categoria?>" maxlength="50"/>
                    <button type="submit" class="btn btn-index" name="Salvar" id="btnSalvar">Salvar</button>

                </form>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include_once('include\js.php'); ?>
    <script src="assets/js/categoria.js"></script>
</body>

</html>