<?php include_once('include\config.php');

$pesquisar = limpar(utf8_decode($_GET["pesquisar"]));

$tipo_txt_xls = limpar(utf8_decode($_GET["tipo"]));

switch ($tipo_txt_xls) {
 case "txt":
 $tipo = "text/plain";
 $nome = "exportar_text.txt";
 break;
 case "xls":
 $tipo = "application/vnd.ms-excel";
 $nome = "exportar_excel.xls";
 break;

}

header("Content-type: {$tipo}");
header("Content-type: application/force-download");
header("Content-Disposition: attachment; filename={$nome}");
header("Pragma: no-cache");

$find = " AND (tp.id_perfil LIKE '%".$pesquisar."%' OR nm_perfil LIKE '%".$pesquisar."%' OR nm_email LIKE '%".$pesquisar."%' OR dt_nascimento LIKE '%".$pesquisar."%' OR nm_tipo LIKE '%".$pesquisar."%' OR nm_subtipo LIKE '%".$pesquisar."%')";

                                    //$conn, FIND, HAVING, WITH_ID
$execute_select = get_perfil($conn, $find, false, false);

if($tipo_txt_xls == 'xls'):
?>

<TABLE BORDER=1>
  <TR>
    <TD>ID Perfil</TD>
    <TD>Imagem</TD>
    <TD>Nome</TD>
    <TD>E-mail</TD>
    <TD>Data de nascimento</TD>
    <TD>Categorias</TD>
    <TD>Tipo</TD>
    <TD>Subtipo</TD>
    <TD>Tags</TD>
  </TR>
  <?php 
endif;
//FINAL TIPO tipo_txt_xls - criar o cabeçalho'
  if($execute_select->rowCount() > 0){
    foreach($execute_select as $row):

      $id_perfil = $row["id_perfil"];
      $nm_perfil = utf8_encode($row["nm_perfil"]);
      $nm_email = $row["nm_email"];
      $dt_nascimento = trataData($row["dt_nascimento"], 2);
      $ds_foto = $row["ds_foto"];
      $nm_categoria = utf8_encode($row["categorias"]);
      $nm_tipo = utf8_encode($row["nm_tipo"]);
      $nm_subtipo = utf8_encode($row["nm_subtipo"]);
      $tags = utf8_encode($row["tags"]);

      if($tipo_txt_xls == 'xls'):
    ?>
    <TR>
      <TD VALIGN=TOP><?=$id_perfil?></TD>
      <TD VALIGN=TOP><?=$ds_foto?></TD>
      <TD VALIGN=TOP><?=$nm_perfil?></TD>
      <TD VALIGN=TOP><?=$nm_email?></TD>
      <TD VALIGN=TOP><?=$dt_nascimento?></TD>
      <TD VALIGN=TOP><?=$nm_categoria?></TD>
      <TD VALIGN=TOP><?=$nm_tipo?></TD>
      <TD VALIGN=TOP><?=$nm_subtipo?></TD>
      <TD VALIGN=TOP><?=$tags?></TD>
    </TR>
    <?php elseif($tipo_txt_xls = "txt") : ?>
          ID Perfil: <?=$id_perfil?> 
          Imagem: <?=$ds_foto?>
          Nome: <?=$nm_perfil?>
          E-mail: <?=$nm_email?>
          Data de nascimento: <?=$dt_nascimento?>
          Categorias: <?=$nm_categoria?>
          Tipo: <?=$nm_tipo?>
          Subtipo: <?=$nm_subtipo?>
          Tags:<?=$tags?>
          =================================================
    <?php
      endif;
    endforeach;
  }else{


    $HAVING = " HAVING (categorias LIKE '%".$pesquisar."%' OR tags LIKE '%".$pesquisar."%')";

                                    //$conn, FIND, HAVING, WITH_ID
    $execute_select_having = get_perfil($conn, false, $HAVING ,false);

    if($execute_select_having->rowCount() > 0){
      foreach($execute_select_having as $row):

        $id_perfil = $row["id_perfil"];
      $nm_perfil = utf8_encode($row["nm_perfil"]);
      $nm_email = $row["nm_email"];
      $dt_nascimento = trataData($row["dt_nascimento"], 2);
      $ds_foto = $row["ds_foto"];
      $nm_categoria = utf8_encode($row["categorias"]);
      $nm_tipo = utf8_encode($row["nm_tipo"]);
      $nm_subtipo = utf8_encode($row["nm_subtipo"]);
      $tags = utf8_encode($row["tags"]);
      
      if($tipo_txt_xls == 'xls'):
      ?>
      <TR>
        <TD VALIGN=TOP><?=$id_perfil?></TD>
        <TD VALIGN=TOP><?=$ds_foto?></TD>
        <TD VALIGN=TOP><?=$nm_perfil?></TD>
        <TD VALIGN=TOP><?=$nm_email?></TD>
        <TD VALIGN=TOP><?=$dt_nascimento?></TD>
        <TD VALIGN=TOP><?=$nm_categoria?></TD>
        <TD VALIGN=TOP><?=$nm_tipo?></TD>
        <TD VALIGN=TOP><?=$nm_subtipo?></TD>
        <TD VALIGN=TOP><?=$tags?></TD>
      </TR>
      <?php elseif($tipo_txt_xls = "txt") : ?>
          ID Perfil: <?=$id_perfil?>
          Imagem: <?=$ds_foto?>
          Nome: <?=$nm_perfil?>
          E-mail: <?=$nm_email?>
          Data de nascimento: <?=$dt_nascimento?>
          Categorias: <?=$nm_categoria?>
          Tipo: <?=$nm_tipo?>
          Subtipo: <?=$nm_subtipo?>
          Tags:<?=$tags?>
          =================================================
    <?php
      endif; 
      endforeach;

    }else{
       if($tipo_txt_xls == 'xls'):
      ?>
      <tr><td colspan='9'>Nenhum resultado encontrado!<td></tr>
      <?php elseif($tipo_txt_xls = "txt") : ?>
          Nenhum resultado encontrado!
    <?php
      endif;    
    }
  }
  if($tipo_txt_xls == 'xls'):    ?>
</TABLE>
<?php endif;?>