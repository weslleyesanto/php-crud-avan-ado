<?php
//Arquivo criado para criar funções que seram usadas dentro do sistema

/*################################################################################################'
'########################### INICIO FUNÇÕES PRINCIPAIS DO SISTEMA ################################'
'################################################################################################*/

function trataData($string, $tipo = 0) {
	if ($string && empty($string) == false) {

		switch ($tipo) {
			case 1: 

			$ex = explode('/', $string);
							//$dia, $mes, $ano
			if(validarData($ex[0], $ex[1], $ex[2])){
				return $ex[2] . '-' . $ex[1] . '-' . $ex[0];
			}else{
				return false;
			}
			break;
			
			case 2:
			$ex = explode('-', substr($string, 0, 10));
			$horario = substr($string, 11, strlen($string)); 
							//$dia, $mes, $ano
			if(validarData($ex[2], $ex[1], $ex[0])){
				return $ex[2] . '/' . $ex[1] . '/' . $ex[0];
			}else{
				return false;
			}
			break;
			
			case 3:
			$ex_data = explode('-', substr($string, 0, 10));
			$horario = substr($string, 11, strlen($string));
			return $ex_data[2] . '/' . $ex_data[1] . '/' . $ex_data[0] . ' ' . $horario;
			break;
			
			case 4:
			$ex_data = explode('-', substr($string, 0, 10));
			$horario = substr($string, 11, strlen($string));
			return $ex_data[2] . '-' . $ex_data[1] . '-' . $ex_data[0] . ' ' . $horario;
			break;
			index:
			$ex = explode('-', $string);
			return $ex[2] . '-' . $ex[1] . '-' . $ex[0];
			break;
		}
	}
}

function validarData($dia, $mes, $ano){
	
	if($dia <= 0 OR $dia >= 32){ // VERICA SE DIA É MENOR IGUAL A ZERO OU MAIOR IGUAL A 32
		return false;
	}else if($mes <= 0 OR $mes >=13){//VERIFICA MES SE É MENOR IGUAL A ZERO OU MAIOR IGUAL A 13 
		return false;
	}else if($ano <= 0){ // VERICA SE ANO É MENOR IGUAL A ZERO
		return false;	
	}else{
		return true;
	}
}


function delTree($dir) { 
	$files = array_diff(scandir($dir), array('.','..')); 
	foreach ($files as $file) { 
		(is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
	} 
	return rmdir($dir); 
}

function limpar($string){

	$string = trim($string);
	$string =str_replace("'","",$string);//aqui retira aspas simples <'>
	$string =str_replace("\\","",$string);//aqui retira barra invertida<\\>
	$string =str_replace("UNION","",$string);//aqui retiro  o comando UNION <UNION>

	$banlist = array('"',"“","”", " insert", " select", " update", " delete", " distinct", " having", " truncate", "replace"," handler", " like", " as ", "or ", "procedure ", " limit", "order by", "group by", " asc", " desc","'","union all", "=", "'", "(", ")", "<", ">", " update", "-shutdown",  "--", "'", "#", "$", "%", "¨", "&", "'or'1'='1'", "--", " insert", " drop", "xp_", "*", " and");

	if(preg_match("/[a-zA-Z0-9]+/", $string)){
		$string = trim(str_replace($banlist,'', $string));
	}

	return $string;

}

function validaEmail($email){
	$er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
	if (preg_match($er, $email)){
		return true;
	} else {
		return false;
	}
}

function select($conn, $TABELA, $PARAM = false, $WHERE, $INNER = false, $GROUP_BY = false, $HAVING = false, $ORDER_BY = false, $DEBUG = false){
		//VERIFICA SE PARAM É FALSE'
	if($PARAM == false){
		$PARAM = "";
	}

		//VERIFICA SE INNER É FALSE'
	if($INNER == false){
		$INNER = "";
	}

	//VERIFICA SE GROUP_BY É FALSE'
	if ($GROUP_BY == false){
		$GROUP_BY = "";
	}
	//VERIFICA SE HAVING É FALSE'
	if ($HAVING == false){
		$HAVING = "";
	}

	//VERIFICA SE GROUP_BY É FALSE'
	if ($ORDER_BY == false){
		$ORDER_BY = "";
	}

		//MONTANDO QUERY'
	$QUERY = "SELECT * " . $PARAM . " FROM " . $TABELA . $INNER . $WHERE . $GROUP_BY.$HAVING.$ORDER_BY;

	if($DEBUG){
		die('###SELECT>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function update($conn, $TABELA, $SET, $WHERE, $DEBUG = false){

	//MONTANDO QUERY'
	$QUERY = 'UPDATE ' . $TABELA . $SET .$WHERE;

	if($DEBUG){
		die('###UPDATE>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function delete($conn, $TABELA, $WHERE, $DEBUG = false){

		//MONTANDO QUERY'
	$QUERY = 'DELETE FROM '. $TABELA . $WHERE;

	if($DEBUG){
		die('###DELETE>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function insert($conn, $TABELA, $PARAM, $LAST_ID = FALSE, $DEBUG = false){

	$QUERY = 'INSERT INTO ' . $TABELA . $PARAM;

	if($DEBUG){
		die('###INSERT>>> '.$QUERY);
	}

	if($LAST_ID){
		$conn->query($QUERY);
		return $conn->lastInsertId($LAST_ID);
	}

	return $conn->query($QUERY);
}

function criarPasta($caminho, $pasta){
	
	if(!@file_exists($caminho."/".$pasta)){
		if(!@mkdir($caminho."/".$pasta, 0777)){
			return true;
		}else{
			return false;
		}
	}
	return false;
}

function sendEmail($remetente = false ,$remetente_nome,$destino,$copia =false ,$assunto,$corpo){
	
	//Inicia a classe PHPMailer
	$mail = new PHPMailer();

	// Define os dados do servidor e tipo de conexão
	$mail->IsSMTP(); // Define que a mensagem será SMTP
	$mail->Host = "smtp.kbrtec.com.br"; // Endereço do servidor SMTP
	$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
	$mail->Username = 'weslley.santo@kbrtec.com.br'; // Usuário do servidor SMTP
	$mail->Password = 'wesley2015so'; // Senha do servidor SMTP

	// Define o remetente
	if($remetente != false){
		$mail->From = $remetente; // Seu e-mail
		$mail->FromName = "No Reply"; // Seu nome
	}else{
		$mail->From = "no-reply@kbrtec.com.br"; // Seu e-mail
		$mail->FromName = "No Reply"; // Seu nome
	}

	// Define os destinatário(s)
	$mail->AddAddress($destino, $remetente_nome);
	//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
	//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta

	// Define os dados técnicos da Mensagem
	$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
	$mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)

	// Define a mensagem (Texto e Assunto)
	$mail->Subject  = $assunto; // Assunto da mensagem
	$mail->Body = $corpo;
	
	// Define os anexos (opcional)
	//$mail->AddAttachment("c:/temp/documento.pdf", "novo_nome.pdf");  // Insere um anexo

	// Envia o e-mail
	return $mail->Send();

	// Limpa os destinatários e os anexos
	$mail->ClearAllRecipients();
	$mail->ClearAttachments();

}

function delete_imagem($caminho_arquivo){
	
	if(file_exists($caminho_arquivo)){
		unlink($caminho_arquivo);
		return true;
	}else{
		return false;
	}
}

/*################################################################################################'
'########################### FINAL FUNÇÕES PRINCIPAIS DO SISTEMA ################################'
'################################################################################################'

'################################################################################################'
'################################## INICIO FUNÇÕES DO SISTEMA ####################################'
'################################################################################################*/

//'################ INICIO PERFIL'
function get_perfil($conn, $find = false, $HAVING = false, $id = false){
	
	$TABELA = "tb_tags tg, tb_categoria tc, tb_perfil tp";
	$PARAM = " , tp.id_perfil, tp.nm_perfil, tp.nm_email, tp.dt_nascimento, tp.ds_foto, GROUP_CONCAT(distinct(tc.nm_categoria)) as categorias, tt.nm_tipo, tst.nm_subtipo, GROUP_CONCAT(distinct(tg.nm_tags)) as tags";
	$WHERE =" WHERE  tpc.id_categoria = tc.id_categoria AND tpt.id_tags = tg.id_tags AND tp.ic_status = '1' AND tc.ic_status = '1' AND tg.ic_status = '1' AND tt.ic_status = '1' AND tst.ic_status = '1' AND tp.ic_linguagem = 'P'";
	$INNER = " 	INNER JOIN tb_tipo tt ON tp.id_tipo = tt.id_tipo INNER JOIN tb_subtipo tst ON tp.id_subtipo = tst.id_subtipo INNER JOIN tb_perfil_categoria tpc ON tp.id_perfil = tpc.id_perfil INNER JOIN tb_perfil_tags tpt ON tp.id_perfil = tpt.id_perfil"; 
	$GROUP_BY = " GROUP BY tp.id_perfil";

	if($find){
		$WHERE .= $find;
	}

	if($id){
		$WHERE .= $id;
	}

					//$conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	return  select($conn, $TABELA, $PARAM, $WHERE, $INNER, $GROUP_BY, $HAVING, false, false);

}

function verificaEmail($conn, $nm_email, $id_perfil = false){

	$TABELA = "tb_perfil";
	$WHERE = " WHERE nm_email = '".$nm_email."'";
	
	if($id_perfil){
		$WHERE .= " AND id_perfil <> ".$id_perfil;		
	}
					//$conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	$retorno = select($conn, $TABELA, false, $WHERE, false, false, false, false, false);
	
	return $retorno->rowCount();
}

function validacao_perfil($conn, $dados_post, $dados_file, $DEBUG = false){

	if($DEBUG){
		echo('<pre>'); 
		print_r($dados_post); 
		print_r($dados_file); 
		die(); 
	}
	//ATTR VALOR DO ARRAY A VARIAVEIS
	$id_perfil = $dados_post['id_perfil'];
	$email = $dados_post['email'];

	$retorno['res'] = 'ok';

	$dt_nascimento = limpar(trataData($dados_post['dt_nascimento'], 1));

	if($dados_post['nm_perfil'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o campo nome!';
	}

	if($email == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o campo e-mail!';	
	}

	if($id_perfil == 0){
    									//conn, e-mail, id_perfil
		$verifica_email = verificaEmail($conn, $email, false);

		if($verifica_email > 0){
			$retorno['res'] = 'error';
			$retorno['msg'] = 'Já existe um e-mail cadastrado com esse endereço!';
		}
	}

	if($id_perfil != 0){
										//conn, e-mail, id_perfil
		$verifica_email = verificaEmail($conn, $email, $id_perfil);
		if($verifica_email > 0){
			$retorno['res'] = 'error';
			$retorno['msg'] = 'Já existe um e-mail cadastrado com esse endereço!';
		}

	}

	if(!$dt_nascimento || $dt_nascimento == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Formato da data incorreto!';
	}

	if($dados_post['id_categoria'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Selecione ao menos uma categoria!';
	}

	if($dados_post['id_tipo'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Selecione um tipo!';
	}

	if($dados_post['id_subtipo'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Selecione um subtipo!';
	}

	if($dados_post['nm_tags'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Selecione ao menos uma tag!';
	}

	/*$privatekey = "6LcwKQcTAAAAAF04H8GMiZKCy56ATV7GUR6Vw3MQ";
  	$resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $dados_post["recaptcha_challenge_field"], $dados_post["recaptcha_response_field"]);
  	
  	if (!$resp->is_valid) {
    	$retorno['res'] = 'error';
    	$retorno['msg'] = 'O reCAPTCHA não foi digitado corretamente. Erro: '. $resp->error;
    }*/

    if(isset($retorno['res']) && $retorno['res'] == 'error'){
    	echo(json_encode($retorno));
    	exit;
    }

}

function crop($x1, $y1, $width, $height, $caminho, $nome_foto, $DEBUG = FALSE){

	if ($DEBUG) {
		echo('X1: '. $x1);
		echo('<br/>');
		echo('Y1: '. $y1);
		echo('<br/>');
		echo('width: '. $width);
		echo('<br/>');
		echo('height: '. $height);
		echo('<br/>');
		echo('caminho: '. $caminho);
		echo('<br/>');
		echo('nome_foto: '. $nome_foto);
		echo('<br/>');
		die('DEBUG LIGADO!');
	}

	$image = WideImage::load($caminho. '/'.$nome_foto);
	// cortar imagem
	$image = $image->crop($x1, $y1, $width, $height);
    // Salva a imagem em um arquivo (novo ou não)
	if ($image->saveToFile($caminho . '/crop_' . $nome_foto)) {
		return true;
	}else{
		return false;
	}
}

function thumb($caminho_original, $pasta, $width, $height, $nome_foto, $DEBUG = false){

	if ($DEBUG) {
		echo('caminho_original: '. $caminho_original);
		echo('<br/>');
		echo('pasta: '. $pasta);
		echo('<br/>');
		echo('width: '. $width);
		echo('<br/>');
		echo('height: '. $height);
		echo('<br/>');
		echo('nome_foto: '. $nome_foto);
		die('DEBUG LIGADO!');
	}

	//carrega imagem
	$image = WideImage::load($caminho_original. '/crop_'.$nome_foto);
	
	// Redimensiona a imagem
	$image = $image->resize($width, $height);
	
	// Salva a imagem em um arquivo (novo ou não)
	if ($image->saveToFile($caminho_original.$pasta . '/' . $nome_foto)) {
		return true;
	}else{
		return false;
	}

}

//'FINAL PERFIL #########################'

//'################ INICIO CATEGORIA'
function get_categoria($conn){
	$TABELA = "tb_categoria ";
	$WHERE = " WHERE ic_status = '1'";
	$ORDER_BY = " ORDER BY nm_categoria ASC";
				//$conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	return select($conn, $TABELA, false, $WHERE, false, false, false, $ORDER_BY, false);
}

function verificaCategoria($conn, $nm_categoria, $id_categoria = false){

	$TABELA = "tb_categoria";
	$WHERE = " WHERE nm_categoria = '".$nm_categoria."' AND ic_status = '1'";
	
	if($id_categoria){
		$WHERE .= " AND id_categoria <> ".$id_categoria;
	}

					//$conn,TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	$retorno = select($conn, $TABELA, false, $WHERE, false, false, false, false, false);
	
	return $retorno->rowCount();

}

function validacao_categoria($conn, $dados_post, $DEBUG = false){

	if($DEBUG){
		echo('<pre>'); 
		print_r($dados_post); 
		die(); 
	}
	//ATTR VALOR DO ARRAY A VARIAVEIS
	$id_categoria = $dados_post['id_categoria'];
	$nm_categoria = $dados_post['nm_categoria'];
	
	$retorno['res'] = 'ok';

	if($nm_categoria == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o nome da categoria!';
	}

	if($id_categoria == 0){
    											//conn, nome_categoria, id_perfil
		$verificar_categoria = verificaCategoria($conn, $nm_categoria, false);

		if($verificar_categoria > 0){
			$retorno['res'] = 'error';
			$retorno['msg'] = 'Já existe uma categoria com esse nome!';
		}
	}

	if($id_categoria != 0){
										//conn, e-mail, id_perfil
		$verificar_categoria = verificaCategoria($conn, $nm_categoria, $id_categoria);
		if($verificar_categoria > 0){
			$retorno['res'] = 'error';
			$retorno['msg'] = 'Já existe uma categoria com esse nome!';
		}

	}

	if(isset($retorno['res']) && $retorno['res'] == 'error'){
		echo(json_encode($retorno));
		exit;
	}

}
//'FINAL CATEGORIA #########################'

//'######################### INICIO TIPOS '
function get_tipos($conn){

	$TABELA = "tb_tipo ";
	$WHERE = " WHERE ic_status = '1' ";
	$ORDER_BY = " ORDER BY nm_tipo ASC";
							//conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	return select($conn, $TABELA, false, $WHERE, false, false, false, $ORDER_BY, FALSE);

}
//'FINAL TIPOS #########################'

//'######################### INICIO TAGS '
function get_tags($conn){

	$TABELA = "tb_tags ";
	$WHERE = " WHERE ic_status = '1' ";
	$ORDER_BY = " ORDER BY nm_tags ASC";
				//conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	return select($conn, $TABELA, false, $WHERE, false, false, false, $ORDER_BY, FALSE);

}
//'FINAL TAGS #########################'

//''######################### INICIO PERFIL CATEGORIA'
function get_perfil_categoria($conn, $id_perfil){

	$TABELA = "tb_perfil_categoria tpc";
	$WHERE = " WHERE tpc.id_perfil = " . $id_perfil;

				//conn,TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	return select($conn, $TABELA, false, $WHERE, false, false, false, false, false);

}

function insert_perfil_categoria($conn, $id_perfil, $id_categoria){

	$TABELA = "tb_perfil_categoria ";

	delete($conn, $TABELA, "WHERE id_perfil = ".$id_perfil, false);

	$id_categoria_explode = explode(',', $id_categoria);

	foreach ($id_categoria_explode as $value_id_categoria) {
		$PARAM = "(id_perfil, id_categoria) VALUES(".$id_perfil.", ".$value_id_categoria.")";
		insert($conn, $TABELA, $PARAM, false, false);
	}

}

//'FINAL PERFIL CATEGORIA #########################'

//'######################### INICIO PERFIL TAGS'
function get_perfil_tags($conn, $id_perfil){

	$TABELA = "tb_perfil_tags tpt";
	$WHERE = " WHERE tpt.id_perfil = " . $id_perfil;

				//conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	return select($conn, $TABELA, false, $WHERE, false, false, false, false, false);

}

function insert_perfil_tags($conn, $id_perfil, $id_tags){

	$TABELA_PERFIL_TAGS = "tb_perfil_tags ";
	$TABELA_TAGS = "tb_tags";

	delete($conn, $TABELA_PERFIL_TAGS, "WHERE id_perfil = ".$id_perfil, false);

	$tags_explode = explode(',', $id_tags);

	foreach ($tags_explode as $nm_tag) {

		$WHERE = " WHERE nm_tags = '" . $nm_tag . "'";
		
							//conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
		$get_id_tag = select($conn, $TABELA_TAGS, false, $WHERE, false, false, false, false, false);

		if($get_id_tag->rowCount() > 0){

			foreach($get_id_tag as $tag){

				$id_tags = $tag["id_tags"];

				$PARAM = "(id_perfil, id_tags) VALUES(".$id_perfil.", ".$id_tags.")";
   				//INSERIR AS TAGS QUE JÁ EXISTEM NA TABELA TB_TAGS EM TB_PERFIL_TAGS'
   						//conn, TABELA, PARAM, DEBUG'	
				insert($conn, $TABELA_PERFIL_TAGS, $PARAM, false);
			}
		}else{
			$PARAM = "";
			$PARAM = "(nm_tags) VALUES('".utf8_decode($nm_tag)."')";
			//INSERINDO NOVAS TAGS E RETORNANDO O ULTIMO ID'        							
        						//conn, TABELA, PARAM, retorno, DEBUG'
        	$last_id_tag = insert($conn, $TABELA_TAGS, $PARAM, 'id_tags', false);

        	$PARAM = "";
        	$PARAM = "(id_perfil, id_tags) VALUES(".$id_perfil.", ".$last_id_tag.")";
   				//INSERIR AS TAGS QUE JÁ EXISTEM NA TABELA TB_TAGS EM TB_PERFIL_TAGS'
   					//conn, TABELA, PARAM, DEBUG'	
			insert($conn, $TABELA_PERFIL_TAGS, $PARAM, false);
		}
	}

}

//'FINAL PERFIL TAGS #########################'

/*'################################################################################################'
'#################################### FINAL FUNÇÕES DO SISTEMA ####################################'
'##################################################################################################*/
?>