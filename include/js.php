<script src="assets/js/jquery.js"></script>

<!-- SCRIPTS GENERICOS DO SISTEMA -->
<script src="assets/js/script.js"></script>

<!-- JMASKED -->
<script src="assets/js/jquery.maskedinput.min.js"></script>

<!-- INICIO TABLE SORTER -->
<script src="assets/js/jquery.tablesorter.js"></script>
<script src="assets/js/jquery.tablesorter.widgets.js"></script>
<!-- FINAL TABLE SORTER -->

<script src="assets/js/jquery-ui.min.js"></script>

<!-- INICIO CHOSEN -->
<script src="assets/js/chosen.proto.min.js"></script>
<script src="assets/js/chosen.jquery.min.js"></script>
<!-- FINAL CHOSEN -->

<!-- JCROP -->
<script src="assets/js/jquery.Jcrop.min.js"></script> 

<!-- tagedit -->
<script src="assets/js/jquery.autoGrowInput.js"></script> 
<script src="assets/js/jquery.tagedit.js"></script> 
