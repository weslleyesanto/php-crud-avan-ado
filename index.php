<?php include_once('include\config.php');
                            //conn, find, HAVING, id
$execute_select = get_perfil($conn, false, FALSE, false); 

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Perfil</title>
    <?php include_once('include\css.php'); ?>
</head>

<body>

    <div id="wrapper">

     <?php include_once('include\menu.php'); ?>

     <div id="page-wrapper">


        <div class="container-fluid">
            <h1>Listagem Perfil</h1>
            <div id="alert" style="display:none;"> </div>
            
            <div id="">
                <a href="perfil_form.php?q=s&acao=add" title="Adicionar Perfil">Adicionar</a>
                <br/><br/>
                Filtro: 
                <!--<input type="search" name="pesquisar" id="pesquisar" class="pesquisar selectable" required="required" maxlength="100" value="" data-column="all"/>-->
                <input class="search selectable" type="search" placeholder="Pesquisar" data-column="all" id="pesquisar">

                Exportar: 
                <a href="exportar.php?tipo=xls&pesquisar" title="Exportar Excel" id="exportar_excel">
                    <img src="assets/imagem/icone/icon-xls.png" alt="Exportar Excel" title="Exportar Excel">
                </a>
                <a href="exportar.php?tipo=txt&pesquisar" title="Exportar Text" id="exportar_txt">
                    <img src="assets/imagem/icone/icon-txt.png"alt="Exportar txt" title="Exportar txt"></button>
                </a>

                <table id="listaPerfil" class="tablesorter">
                    <?php  if($execute_select->rowCount() > 0){ ?>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Thumb</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Data de nascimento</th>
                            <th>Categoria</th>
                            <th>Tipo</th>
                            <th>Subtipo</th>
                            <th>Tags</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php foreach($execute_select as $row): 
                       $id_perfil = $row["id_perfil"];
                       $nm_perfil = utf8_encode($row["nm_perfil"]);
                       $nm_email = $row["nm_email"];
                       $dt_nascimento = trataData($row["dt_nascimento"], 2);
                       $ds_foto = $row["ds_foto"];
                       $nm_categoria = utf8_encode($row["categorias"]);
                       $nm_tipo = utf8_encode($row["nm_tipo"]);
                       $nm_subtipo = utf8_encode($row["nm_subtipo"]);
                       $tags = utf8_encode($row["tags"]);
                       ?>
                       <tr data-id="<?=$id_perfil?>" modulo="perfil" page="index">
                        <td><?=$id_perfil?></td>
                        <td>
                            <img src="<?=$RELATIVO_IMAGEM_PERFIL.$id_perfil.$pasta_thumb.$ds_foto?>" alt="<?=$nm_perfil?>" title="<?=$nm_perfil?>" id="caminho_foto" />
                        </td>
                        <td><?=$nm_perfil?></td>
                        <td><?=$nm_email?></td>
                        <td><?=$dt_nascimento?></td>
                        <td><?=$nm_categoria = $nm_categoria == '' ? 'Sem categoria' : $nm_categoria ?></td>
                        <td><?=$nm_tipo?></td>
                        <td><?=$nm_subtipo?></td>
                        <td><?=$tags?></td>
                        <td id="acoes">
                            <a href="perfil_form.php?q=s&acao=alterar&id=<?=$id_perfil?>" title="Alterar" class="alterar">
                                Alterar
                            </a>
                            <a href="perfil_form.php?q=s&acao=visualizar&id=<?=$id_perfil?>" title="Visualizar">Visualizar</a>
                            <a href="#" title="Excluir" class="excluir">Excluir</a>
                            <a href="#" title="Notificar" class="notificar">Notificar</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php }else{ ?>
        <tr>
            <td colspan="6">Nenhum registro encontrado!</td>
        </tr>
        <?php }?>
    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include_once('include\js.php'); ?>
<script src="assets/js/perfil.js"></script>
</body>

</html>
