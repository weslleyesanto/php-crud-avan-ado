<?php include_once('include\config.php');
include_once('include\class.phpmailer.php');
include_once('include\class.smtp.php');
include_once('include\wideimage\WideImage.php');

$url = "index.php";

if(isset($_POST)){
					//$conn, $dados_post, $dados_file, $DEBUG
	validacao_perfil($conn, $_POST, $_FILES, false); 

    //'PEGANDO CAMPOS PASSADOS DO FORMULÁRIO PARA AS VARIÁVEIS'
	$nm_perfil = limpar(utf8_decode($_POST["nm_perfil"]));
	$email = limpar($_POST["email"]);
	$dt_nascimento = limpar(trataData($_POST["dt_nascimento"], 1));
	
	$foto = @$_FILES["foto"];
	$x1 = limpar($_POST["x1"]);
	$y1 = limpar($_POST["y1"]);
	$width = limpar($_POST["width"]);
	$height = limpar($_POST["height"]);
	$ds_foto = limpar($_POST["ds_foto"]);

	$id_categoria = limpar($_POST["id_categoria"]);
	$id_tipo = limpar($_POST["id_tipo"]);
	$id_subtipo = limpar($_POST["id_subtipo"]);
	$tags = limpar($_POST["nm_tags"]);

	$id_perfil = limpar($_POST["id_perfil"]);

	$modulo = "Perfil";
	$TABELA = "tb_perfil";

    //'CAMINHA DO ARQUIVO
	$file_path  = "assets/imagem/perfil/";

	if($ds_foto == ""){ //INSERIR NOVO REGISTRO'

		//VERIFICA SE FOTO ESTÁ IGUAL A VAZIO'
		if ($foto['size'] == "") {
			$retorno = array('res' => 'error','msg' => 'Por favor, indique um arquivo para upload!');
			echo (json_encode($retorno));
			die();
		} else {

			$nome_foto = limpar($foto['name']);
			$novo_nome = md5(date('Y-m-d H:i:s')).'.jpg';

				//'PREPARANDO PARAMETRO PARA INSERIR NO PERFIL
			$PARAM = "(nm_email, nm_perfil, dt_nascimento, ds_foto, id_tipo, id_subtipo, ic_linguagem) VALUES ('" . $email ."', '" . $nm_perfil ."', '" .$dt_nascimento ."', '" . $novo_nome ."', " . $id_tipo .", " . $id_subtipo .", 'P')";

			$insert_row = insert($conn, $TABELA, $PARAM, 'id_perfil', false);
			//$insert_row = 27;

				if ($insert_row) { //se inseriu retorna TRUE

					//'CHAMANDO FUNÇÃO PARA INSERIR EM PERFIL_CATEGORIA'
					insert_perfil_categoria($conn, $insert_row, $id_categoria);
					//CHAMANDO FUNÇÃO PARA INSERIR EM PERFIL_TAGS'
					insert_perfil_tags($conn, $insert_row, $tags);

					//'CRIAR A PASTA COM O ID DO USUÁRIO
					$pasta_id = criarPasta($file_path, $insert_row);

				    //VERIFICA SE CRIOU A PASTA COM ID DO PERFIL'
					
	                if (!$pasta_id) { //CRIOU A PASTA COM O ID'
		                #INICIO UPLOAD DE IMAGEM PARA O SERVIDOR#'
		                if (!@move_uploaded_file($foto['tmp_name'], $file_path . $insert_row . '/' . $novo_nome)) {
		                	$retorno = array('res' => 'error','msg' => 'Problema ao mover arquivo!');
		                }
			                #FINAL UPLOAD DE IMAGEM PARA O SERVIDOR#'

				    		//CORTAR IMAGEM						x1,y1,width,height, caminho, nome_arquivo, DEBUG)
		                $retorno_crop = crop($x1, $y1, $width, $height, $file_path . $insert_row, $novo_nome, false);

						if($retorno_crop){
							$retorno = array('res' => 'error','msg' => 'Problema ao cortar imagem!');
							echo(json_encode($retorno));
							die();
						}else{
							//'CRIANDO A PASTA THUMN DENTRO DA PASTA COM ID DO PERFIL'
		                	$pasta_thumb = criarPasta($file_path . $insert_row, "/thumb/");

							//'VERIFICA SE CRIAR OU PASTA THUMB'
			                if ($pasta_thumb) {
			                	$retorno = array('res' => 'error','msg' => 'Problema ao criar pasta do THUMB!');
			                	echo(json_encode($retorno));
								die();
			                } else {

			                					//caminho_original, pasta, $width, $heigth, nome_foto, debug
		                		$retorno_thumb = thumb($file_path . $insert_row, '/thumb/', 90, 90, $novo_nome, false);

				                        // Salva a imagem em um arquivo (novo ou não)
			                	if (!$retorno_thumb) {

				                        #INICIO ENVIO DE E-MAIL#'
			                		$nome_remetente = "KBRTEC";
			                		$destino        = $email;
			                		$assunto        = "Novo Cadastro";
			                		$corpo          = "Você foi cadastrado com sucesso!";

			                		$envio = sendEmail(false, $nome_remetente, $destino, FALSE, $assunto, $corpo);
				                        #FINAL ENVIO DE E-MAIL#'

			                		if (!$envio) {
			                			$retorno = array('res' => 'error','msg' => 'Problema ao enviar e-mail!');
			                		}
				                         //FINAL VERIFICA SE ENVIOU E-MAIL'
			                	} else {
			                		$retorno = array('res' => 'error','msg' => 'Problema ao redimensionar imagem!');
			                	}
				                    //VERIFICA REDIMENSIONAR
			                }
			                //FINAL VERIFICA SE CRIOU PASTA THUMB'
		               }
		               //FINAL VERIFICA SE FEZ O CROP

	            }else{
	            	$retorno = array('res' => 'error','msg' => 'Problema ao criar pasta do perfil!');
	            }
						//FINAL VERIFICA SE CRIOU A PASTA'

	            $retorno = array('res' => 'ok', 'msg' => "{$modulo} cadastrado com sucesso!",'url' => $url);

	        }else {
	        	$retorno = array('res' => 'error','msg' => "Problema ao inserir novo {$modulo}!");
	        }
			//'FINAL VERIFICA SE INSERIU NA TABELA'
	}
	//FINAL VERIFICA SE FOTO NÃO ESTÁ VAZIA'

	} elseif ($ds_foto != "") {

	        if ($id_perfil != "0") { //'SE ID FOR DIFERENTE DE ZERO É UMA ALTERAÇAO

	            if ($foto['size'] != "") { //'ALTERAR DADOS E FAZER UM NOVO UPLOAD'

		            $file = $file_path . $id_perfil . '/'. $ds_foto;
		            $file_crop = $file_path . $id_perfil . '/crop_'. $ds_foto;
		            $file_thumb = $file_path . $id_perfil . '/thumb/'.$ds_foto;

						//'DELETA IMAGEM PRINCIPAL'
		            if(!delete_imagem($file)){
		            	$retorno = array('res' => 'error','msg' => 'Problema ao excluir imagem principal!');
		            }
						//DELETA IMAGEM CROP'
		            if(!delete_imagem($file_crop)){
		            	$retorno = array('res' => 'error','msg' => 'Problema ao excluir imagem crop!');
		            }
						//DELETA IMAGEM THUMB'
		            if(!delete_imagem($file_thumb)){
		            	$retorno = array('res' => 'error','msg' => 'Problema ao excluir imagem thumb!');
		            }

		            $nome_foto = limpar($foto['name']);
		            $novo_nome = md5(date('Y-m-d H:i:s')).'.jpg';

						#INICIO UPLOAD DE IMAGEM PARA O SERVIDOR#'
		            if (!@move_uploaded_file($foto['tmp_name'], $file_path . $id_perfil . '/' . $novo_nome)) {
		            	$retorno = array('res' => 'error','msg' => 'Problema ao mover arquivo!');
		            }
		           		#FINAL UPLOAD DE IMAGEM PARA O SERVIDOR#'

						//CORTAR IMAGEM						x1,y1,width,height, caminho, nome_arquivo, DEBUG)
		                $retorno_crop = crop($x1, $y1, $width, $height, $file_path . $id_perfil, $novo_nome, false);

						if($retorno_crop){
							$retorno = array('res' => 'error','msg' => 'Problema ao cortar imagem!');
							echo(json_encode($retorno));
							die();
						}else{
							//'CRIANDO A PASTA THUMN DENTRO DA PASTA COM ID DO PERFIL'
		                	$pasta_thumb = criarPasta($file_path . $id_perfil, "/thumb/");

							//'VERIFICA SE CRIAR OU PASTA THUMB'
			                if ($pasta_thumb) {
			                	$retorno = array('res' => 'error','msg' => 'Problema ao criar pasta do THUMB!');
			                	echo(json_encode($retorno));
								die();
			                } else {
			                					//caminho_original, pasta, $width, $heigth, nome_foto, debug
		                		$retorno_thumb = thumb($file_path . $id_perfil, '/thumb/', 90, 90, $novo_nome, false);

			                	if (!$retorno_thumb) {
			                		$retorno = array('res' => 'error','msg' => 'Problema ao redimensiocar imagem!');
		            			}
		            		}
		            		//FINAL VERIFICA PASTA THUMB
		            	}
		            	//FINAL VERIFICA RETORNO DO CROP
		        }
				//'FINAL VERIFICA SE FOTO É DIFERENTE DE VAZIO'

		    	//'INICIO UPDATE
	        	$PARAM = " SET nm_perfil = '".$nm_perfil."', nm_email = '".$email."', dt_nascimento = '".$dt_nascimento."', id_tipo = ".$id_tipo.", id_subtipo = ".$id_subtipo; 

		        if ($foto['size'] != "") {
		        	$PARAM .= ", ds_foto = '" . $novo_nome . "'";
		        }

		        $WHERE          = " WHERE id_perfil = " . $id_perfil;
		                    //conn, TABELA, PARAM, WHERE, DEBUG'
		        $retorno_update = update($conn, $TABELA, $PARAM, $WHERE, false);

		        if ($retorno_update) {

		        	//'CHAMANDO FUNÇÃO PARA INSERIR EM PERFIL_CATEGORIA'
		        	insert_perfil_categoria($conn, $id_perfil, $id_categoria);
					//CHAMANDO FUNÇÃO PARA INSERIR EM PERFIL_TAGS'
		        	insert_perfil_tags($conn, $id_perfil, $tags);

		        	$url     = "perfil_form.php?q=s&acao=alterar&id=" . $id_perfil;
		        	$retorno = array('res' => 'ok','msg' => "{$modulo} alterado com sucesso!",'url' => $url);
		        } else {
		        	$retorno = array('res' => 'error','msg' => "Problema ao alterar {$modulo}!");
		        }
		        //FINAL VERIFICA SE FEZ UPDATE
		}
	    //FINAL VERIFICA SE id perfil É diferente de 0'
	}
	//'FINAL VERIFICA SE DS_FOTO É DIFERENTE DE VAZIO'

	echo(json_encode($retorno));
}else{
	header("location: {$url}");
}
//'FINAL VERIFICA METODO DE REQUISIÇÃO IGUAL A POST'

?>