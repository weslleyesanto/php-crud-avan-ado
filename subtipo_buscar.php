<?php include_once('include\config.php');

if(isset($_POST) && isset($_POST['id_tipo'])){

	$retorno = array();


	$id = limpar($_POST["id_tipo"]);
	$selecionado = '';

	if(isset($_POST["id_subtipo_perfil"])){
		$id_subtipo_perfil = limpar($_POST["id_subtipo_perfil"]);

		if ($id_subtipo_perfil == ""){
			$id_subtipo_perfil = 0;
		}
	}


	$TABELA = "tb_subtipo ";
	$WHERE = " WHERE id_tipo = " . $id . " AND ic_status = '1' ";
	$ORDER_BY = " ORDER BY nm_subtipo ASC";

							     //'conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	$execute_query = select($conn, $TABELA, false, $WHERE, false, false, false, $ORDER_BY, false);

   	if($execute_query->rowCount() > 0){//VERIFICA SE É MAIOR QUE ZERO
   		$SELECT_OPTION = "";
   		$SELECT_OPTION .= "<option value=''>Selecione um subtipo</option>";
   		foreach($execute_query as $row){

   			$id_subtipo = $row["id_subtipo"];
   			$nm_subtipo = $row["nm_subtipo"];

   			if(isset($id_subtipo_perfil) && $id_subtipo_perfil != 0){
   				$selecionado = $id_subtipo_perfil == $id_subtipo ? 'selected="selected"' : '';
   			}

   			$SELECT_OPTION .= "<option value='".$id_subtipo."' ".$selecionado.">".$nm_subtipo."</option>";
   		}

   		$retorno = array('res' => 'ok', 'OPTION' => "{$SELECT_OPTION}");	
   	}else{
   		$SELECT_OPTION = "<option value=''>Nenhum subtipo encontrado!</option>";
   		$retorno = array('res' => 'ok', 'OPTION' => "{$SELECT_OPTION}");
   	}

   	echo(json_encode($retorno));
   } 
//'FINAL VERIFICA SE O METHOD IS EQUAL POST'


   ?>